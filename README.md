# Tomb mount and copy

Mount a [tomb](https://dyne.org/software/tomb/) encrypted file from an external device and copy a secret into the clipboard.

Work in progress!  
This works for me and now.  
There are better ways to protect your secrets: this script is what I came up, for my personal use.

There are plans to make this script more general (and maybe more secure): this will happen if I need more features myself.

Documentation will follow...
