if [ ! $# -eq 1 ]; then
    echo "Usage: $0 name.tomb"
    exit
fi

TOMB_FILE=$1

MOUNT_EXISTS=0
MOUNTED=0
if [ -d "/mnt/android" ]; then
    MOUNT_EXISTS=1
    MOUNTED=$(grep -q ' /mnt/android ' /proc/mounts && echo 1 || echo 0)
fi

if [ $MOUNT_EXISTS -eq 0 ]; then
    echo "No valid mount points found"
    exit
fi

if [ $MOUNTED -eq 0 ]; then
    mount /mnt/android
fi

if [ ! -d "/mnt/android/SanDisk SD card/Documents/tomb" ]; then
    echo "Cannot find the tomb directory in SanDisk SD card/Documents/"
    exit 0
fi

# use nullglob in case there are no matching files
shopt -s nullglob

# files=($( ls -1 *.tomb ))
# tombs=(*.tomb)
# i=1
# echo "[$i] cancel"
# for f in "${tombs[@]}"; do
#     echo "[$i] $f"
#     i=$((i+=1))
# done
# 
# echo "${#tombs[@]}"
# 
# while :; do
#     read -p "" input
#     [[ $tomb_i =~ ^[0-9]+$ ]] || { echo "Tomb? [0-${#tombs[@]}]"; continue; }
#     if ((number == 0))
#     if ((number >= 0 && number <= 5)); then
#         break
#     fi
#     
# done
# 
# echo $tomb_i

if [ ! -f "/mnt/android/SanDisk SD card/Documents/tomb/$TOMB_FILE.key" ]; then
    echo "Cannot find the key $TOMB_FILE.key"
    exit 0
fi

tomb open -f "$1" -k "/mnt/android/SanDisk SD card/Documents/tomb/$TOMB_FILE.key"

MOUNT_TOMB=$(basename "$TOMB_FILE" ".tomb")
# cat /media/celerato/keepass.txt | xclip
head -n 1 "/media/$MOUNT_TOMB/keepass.txt" | tr -d '\n' | xclip

tomb close

if [ $MOUNTED -eq 0 ]; then
    umount /mnt/android
fi
